import React, { useReducer } from "react";
import "./App.css";
import DigitButton from "./DigitButton";

export const ACTIONS = {
  ADD_DIGIT: "add-digit",
  CHOOSE_OPERATION: "choose-operation",
  CLEAR: "clear",
  DELETE_DIGIT: "delete-digit",
  EVALUATE: "evaluate",
};

function reducer(state, { type, payload }) {
  switch (type) {
    case ACTIONS.ADD_DIGIT:
      return {
        ...state,
        currentOperend: `${state.currentOperend || ""}${payload.digit}`,
      };
    default:
      return state;
  }
}

function App() {
  const [{ currentOperend, previousOparend, Operation }, dispatch] = useReducer(
    reducer,
    {}
  );

  return (
    <div>
      <div className="calculatorGrid">
        <div className="output">
          <div className="previousOparend">
            {previousOparend} {Operation}
          </div>
          <div className="currentOperend">{currentOperend}</div>
        </div>
        <button className="span-two">AC</button>
        <button>DEL</button>
        <DigitButton digit="÷" dispatch={dispatch} />
        <button>1</button>
        <button>2</button>
        <button>3</button>
        <button>*</button>
        <button>4</button>
        <button>5</button>
        <button>6</button>
        <button>+</button>
        <button>7</button>
        <button>8</button>
        <button>9</button>
        <button> - </button>
        <button>.</button>
        <button>0</button>
        <button className="span-two"> = </button>
      </div>
    </div>
  );
}

export default App;
